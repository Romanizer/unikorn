# Unikorn Headphone Amplifier

## Goal
Build a somewhat modular headphone amplifier,
capable of driving at least 2 headphones at different volumes.

The Ultimate GOAL is to switch,
with as little hassle as possible,
between multiple inputs or internal amplifiers.


This project /will be/ heavily inspired by the [Objective 2](https://nwavguy.blogspot.com/2011/08/o2-summary.html)
and aims to be as good, but more versatile.

### Modularity

Each Amplifier module will have certain common pins.
* I2C or SPI for control or module identification.
* Power
    * 3.3V or 5V for logic
    * ±9V analog supply
* Multiple audio in/out channels (or single one that is controller by the main board)

Interface will be Board-2-Board, like a PCIe x1 Connector or M.2 or Mini PCI.
The Interface will NOT be hot swappable.
Interface cards will have a status LED (RGB).
